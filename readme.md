# Overview

This project allows you to torrent, seed, and serve kiwix images. Notably, it comes configured for the Archlinux Wiki, Wikipedia, and Stackoverflow. You can add your own by adding to the `_SOURCE` environment variables in the `setup` service in `docker-compose.yml`.

To obtain the torrent URL, find the appropriate package on [Kiwix's content packages](https://wiki.kiwix.org/wiki/Content_in_all_languages)

## 💻 OS Configuration
On the Beaglebone black, you have a two main choices -- you can install Arch Linux to the internal memory, with the intent of hot-swapping external SD cards, or you can install Arch Linux to the external SD card.

Note, commands below are noted with `# command` if they need root permissions -- enter a root shell with `su`. Commands noted via `$ command` do not require a root shell.

Any time a file needs to be edited, I would recommend using `nano <file>`. If you get a warning about the file not being writable, then open it from a root shell (see above).

### Install to Internal Memory
This is the approach I used, so it is the most tested, but it is slightly more complicated. On the upshot, you can hotswap projects pretty easily!

1. Start by [Installing Arch Linux](https://archlinuxarm.org/platforms/armv7/ti/beaglebone-black) to the internal memory (eMMC)
2. SSH into the installed Arch Linux
3. Set the hostname by editing the `/etc/hostname` file
   1. _Note_ This lets you connect via `ssh <hostname>` or `http://<hostname>` later on
3. Format the SD Card
    1. Insert the SD Card
    2. Use `# gdisk /dev/mmcblk0` and setup a single partition on the entire SD card
    3. Use `# mkfs.ext4 /dev/mmcblk0p1` to format the SD card with ext4. 
3. Set the SD Card to automatically mount on boot by editing the adding this line to `/etc/fstab`
    1. `/dev/mmcblk0p1 /mnt/sdcard ext4 rw 0 0`
    2. This sets the SD card to mount to `/mnt/sdcard`. Make sure to create this directory. If you mount it somewhere else, modify the remaining steps accordingly!
4. Reboot and make sure the hostname is configured and the SD card is automatically mounted
    1. Use `$ lsblk` to see which devices are mounted where
5. Install docker, and set it to use the SD card for storage (fif you don't, you'll run out of memory very fast)
    1. `# pacman -S docker` to install docker
    2. `# nano /etc/docker/dameon.json` to create the docker configuration. Add the content `{ "data-root": "/mnt/sdcard/docker-store" }` and close the file.
    3. Do `# systemctl restart docker` and make sure the above directory gets created.

### Install to SD Card

This is a bit easier, though if you're shuffling multiple projects, you'll have to install the entire OS for each one. Also the Beaglebone black is weird about having to hold down a button when you boot it to boot from SD cards.

1. Start by [Installing Arch Linux](https://archlinuxarm.org/platforms/armv7/ti/beaglebone-black) to the internal memory (eMMC)
2. SSH into the installed Arch Linux
3. Set the hostname by editing the `/etc/hostname` file
   1. _Note_ This lets you connect via `ssh <hostname>` or `http://<hostname>` later on
4. Install docker with `# pacman -S docker`

## 🏃 Running

Once the OS is setup, to run this project, clone this repo onto the external SD card, and then simply launch `start.sh`. This very simple script does only a few things

- Creates the downloads directory to hold the downloaded ZIM files
- If internet connectivity is present, launches the `setup` profile via docker compose
    + This downloads the configured packages
    + It also hosts a webpage for getting status readouts
- After that, it serves the downloaded packages and seeds them to other upsers.

To check the download status (while downloading) or use the downloaded archives (afterwards) simply go to `http://<hostname>` from another computer on the same network.

If you want the project to start on boot and run it in the background (highly recommended), simply modify the included `sdcard.service` System-D service:
- Replace the user with your user
- Replace the working directory with wherever you cloned this repo to.

From there, copy the service to `/etc/systemd/system/` and then do `# systemctl enable sdcard.service`.

## 👷 Troubleshooting

If the System-D service is present, simply check the output from `log.txt` on the SD card either from the BBB, or by plugging it into another computer.

If not using the System-D service, the console output should be sufficient.

[Open a New Issue](https://gitlab.com/moltenbytes32/bbb-wikipedia/-/issues/new) if any issues arise, and I'll do my best to help you through it and document any fixes here.

Happy Hacking!