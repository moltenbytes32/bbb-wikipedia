#!/bin/bash

docker compose stop
docker compose --profile display down --remove-orphans
docker compose --profile serve down --remove-orphans
docker compose --profile setup down --remove-orphans
