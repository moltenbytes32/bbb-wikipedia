#!/bin/bash

mkdir -p downloads

if ping -c4 kiwix.org
then
  docker compose run setup
  docker compose --profile setup down
fi

docker compose --profile serve up
