#!/bin/bash

echo "Downloading Torrents"

env | while IFS='=' read -r name value
do
  if [[ $name == *'_SOURCE' ]];
  then
    file="${name//_SOURCE/}.torrent"
    url="$value"
    echo "Fetching $file from $url"
    curl -L -o "downloads/$file" "$url"
  fi 
done

echo "Starting Torrents"

aria2c --dir=downloads --seed-time=0 --check-integrity=true downloads/*.torrent
